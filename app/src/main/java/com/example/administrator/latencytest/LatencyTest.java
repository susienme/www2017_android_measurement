package com.example.administrator.latencytest;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Timer;
import java.util.TimerTask;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


public class LatencyTest extends Activity {

    private GLSurfaceView mGLView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Log.e("onCreate", "onCreate");

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().getDecorView()
                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE);
        // Create a GLSurfaceView instance and set it
        // as the ContentView for this Activity.
        mGLView = new MyGLSurfaceView(this);
        setContentView(mGLView);
    }


}

class MyGLSurfaceView extends GLSurfaceView {

    private final MyGLRenderer mRenderer;
    private float mScale = 1.0f;
    public MyGLSurfaceView(Context context){
        super(context);

        //this.setScaleX(2f);
        //this.setScaleY(1.0f);



        //this.getHolder().setFixedSize(360,640);
        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);

        mRenderer = new MyGLRenderer();

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(mRenderer);
        this.setRenderMode(RENDERMODE_WHEN_DIRTY);
//        this.setRenderMode(RENDERMODE_CONTINUOUSLY);

        //MyTimerTask myTask = new MyTimerTask();
        //Timer myTimer = new Timer();
//        public void schedule (TimerTask task, long delay, long period)
//        Schedule a task for repeated fixed-delay execution after a specific delay.
//
//        Parameters
//        task  the task to schedule.
//        delay  amount of time in milliseconds before first execution.
//        period  amount of time in milliseconds between subsequent executions.

        //myTimer.schedule(myTask, 600, 4);

    }

    class MyTimerTask extends TimerTask {
        public void run() {
            requestRender();
        }
    }

    public boolean onTouchEvent(MotionEvent e) {
        // MotionEvent reports input details from the touch screen
        // and other input controls. In this case, you are only
        // interested in events where the touch position changed.
//        Log.e("Songtao Touch Event","Get history(" + e.getHistorySize() + ") " + e.getX()+" "+e.getY());
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
//        Log.e("YMH", "TIMING " + System.currentTimeMillis() + " " + SystemClock.uptimeMillis());

        if(e.getHistorySize() == 0) return true;
//        Log.e("YMH", "REDESIGN App received" + e.getHistorySize() + " events");

       // if(true)
       // {
       //     Log.e("Touch Event","Get");
       //     return true;
       // }

        //ViewGroup.LayoutParams params = this.getLayoutParams();

        //params.height = 1280;
        //params.width = 720;

       // this.setLayoutParams(params);


        //this.setScaleX(2f);
        //this.setScaleY(2f);

        float x = e.getX();
        float y = e.getY();

        int lastIndex = e.getHistorySize() - 1;

        for(int i = 0; i <= lastIndex; i++) {
            y = e.getHistoricalY(i);
            Log.e("YMH", "getHistoricalY[" + i + "] = " + y);
        }
        Log.e("YMH","getY(" + e.getY() +")");

        x = e.getHistoricalX(lastIndex);
        y = e.getHistoricalY(lastIndex);
//        Log.e("YMH","last event ( " + e.getHistoricalX(lastIndex) + " " + e.getHistoricalY(lastIndex) +" ) get event( " + e.getX() + " " + e.getY() +" ) using( " + x + " " + y + " )");

        mRenderer.pos_x_prev = mRenderer.pos_x;
        mRenderer.pos_y_prev = mRenderer.pos_y;

        mRenderer.pos_x = x/4;
        mRenderer.pos_y = y/4;


        mRenderer.pos_x = x;
        mRenderer.pos_y = y;



        requestRender();
        /*
        switch (e.getAction()) {
            case MotionEvent.ACTION_MOVE:


        }
        */
       return true;
    }

    public void func(int count){
        Log.e("YMH", "CHECK func_org" + count);
    }

}

class MyGLRenderer implements GLSurfaceView.Renderer {

    public float pos_x = 0;
    public float pos_y = 0;
    public float pos_x_prev = 0;
    public float pos_y_prev = 0;
    public int w = 1;
    public int h = 1;

    public int mProgram = 0;


    private final String vertexShaderCode =
            "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = vPosition;" +
                    "}";

    private final String fragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";

    public static int loadShader(int type, String shaderCode){

        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }


    public int initProgram()
    {
        int vertexShader = MyGLRenderer.loadShader(GLES20.GL_VERTEX_SHADER,
                vertexShaderCode);
        int fragmentShader = MyGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER,
                fragmentShaderCode);

        // create empty OpenGL ES Program
        mProgram = GLES20.glCreateProgram();

        // add the vertex shader to program
        GLES20.glAttachShader(mProgram, vertexShader);

        // add the fragment shader to program
        GLES20.glAttachShader(mProgram, fragmentShader);

        // creates OpenGL ES program executables
        GLES20.glLinkProgram(mProgram);

        return 1;
    }

    public void drawRect(float _x,float _y,float _w,float _h,float _r,float _g, float _b)
    {
        int COORDS_PER_VERTEX = 2;
        int vertexStride = COORDS_PER_VERTEX * 4;
        int vertexCount = 6;

        float triangleCoords[] = {
                _x, _y,
                _x + _w, _y,
                _x, _y + _h,
                _x, _y + _h,
                _x + _w, _y + _h,
                _x + _w, _y
        };

        float dummy[] = {
                0, 0,
                0, 0,
                0, 0,
                0, 0,
                0, 0,
                0, 0,
        };

        float color[] = { _r, _g, _b, 1.0f };

        FloatBuffer vertexBuffer;
        FloatBuffer vertexBuffer_dummy;

        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (number of coordinate values * 4 bytes per float)
                triangleCoords.length * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        ByteBuffer bb_dummy = ByteBuffer.allocateDirect(
                // (number of coordinate values * 4 bytes per float)
                dummy.length * 4);
        bb_dummy.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer_dummy = bb_dummy.asFloatBuffer();
        // add the coordinates to the FloatBuffer
        vertexBuffer.put(triangleCoords);
        vertexBuffer_dummy.put(dummy);
        // set the buffer to read the first coordinate
        vertexBuffer.position(0);
        vertexBuffer_dummy.position(0);


        GLES20.glUseProgram(mProgram);

        // get handle to vertex shader's vPosition member
        int mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Prepare the triangle coordinate data

        GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);
        /*GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer_dummy);*/

        // get handle to fragment shader's vColor member
        int mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");

        // Set color for drawing the triangle
        GLES20.glUniform4fv(mColorHandle, 1, color, 0);

        // Draw the triangle
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }


    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        // Set the background frame color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        initProgram();
    }


    long wallTime = 0;
    int frameCount = 0;
    int frameTotal = 0;
    int mode = 0;
//    boolean bPartialUpdateMode = true;
    boolean bPartialUpdateMode = false;

    public void onDrawFrame(GL10 unused) {
//        Log.e("YMH_LAT", "onDraw");
        long now = System.currentTimeMillis();

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);

        //pos_y = pos_y + 3f;

        if(pos_y>h) pos_y = 0f;

        //YMH: turn off the hidden switch
        /*if(pos_y< 200 && pos_x < 200)
        {
            if(mode == 1) mode = 0;
            else mode = 1;
        }*/

//        GLES20.glViewport(0, 0, w, h);


        if(now-wallTime > 1000)
        {
//            Log.e("FrameRate", "FPS "+ frameCount + "  "+ now+ " "+wallTime);

            frameCount = 0;
            wallTime = now;
        }
        frameCount++;
        frameTotal++;

        // Redraw background color
        float width = 0.01f;
        float markersize = 0.08f;
        //markersize = 0.15f;
        float rate =9.0f / 16.0f;


        //GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);


        //if(frameTotal % 2 ==0)
//        if (pos_y > 1000) return;

        if (bPartialUpdateMode && pos_y != 0) {
            int lowY, height;
            GLES20.glEnable(GLES20.GL_SCISSOR_TEST);

            if (pos_y_prev > pos_y) {
                lowY = (int) pos_y_prev;
                height = (int) (pos_y_prev - pos_y);
            } else {
                lowY = (int) pos_y;
                height = (int) (pos_y - pos_y_prev);
            }

            GLES20.glScissor(0, 2560 - lowY - 100, 1440, height+200);
            if (GLES20.glGetError() == GLES20.GL_NO_ERROR) Log.e("YMH", "SCISSOR WORKS!!");
        }

//        drawRect(-1, -1, 2, 2, 1.0f, 1.0f, 1.0f);
            GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        //else
//            GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);


        if(mode == 1) {
            if (frameTotal % 2 == 0)
                drawRect(-1, -1, 1, 2, 0.0f, 0.0f, 0.0f);
            else
                drawRect(-1, -1, 1, 2, 1.0f, 1.0f, 1.0f);
        }
        //drawRect(-1, -1, 2 * pos_x / w, 2.0f - 2 * pos_y / h, 1.0f, 0.0f, 0.0f);

        //drawRect(-1,1.0f - 2*pos_y/ h-0.5f*width ,2.0f,width,1.0f,1.0f,1.0f);


        float pos = 200;

        /*
        for(int i = 0;i<6;i++) {
            pos += 50f;
            drawRect(-1, 1.0f - 2 * pos / h - 0.5f * width, 2.0f, width, 0.2f, 0.95f, 0.2f);
        }*/


//        drawRect(-1, 1.0f - 2 * pos_y / h - 0.5f * width, 2.0f, width, 0.95f, 0.95f, 0.95f);
        drawRect(-1, 1.0f - 2 * pos_y / h - 0.5f * width, 2.0f, width, 0.9f, 0.9f, 0.9f);
        //drawRect(-1, 1.0f - 2 * pos_y / h - 0.5f * width, 2.0f, width, 0.95f, 0.0f, 0.0f);

        //Line
        //drawRect(-1, 1.0f - 2*pos_y/ h-0.5f*width ,2.0f,width,1.0f,0.0f,0.0f);



/*
        drawRect(1.0f-markersize,1.0f - 2*pos_y/ h-0.5f*markersize*rate,2.0f,markersize*rate,1.0f,0.0f,0.0f);

        for(int i = -10;i<10;i++)
        {
            drawRect(1.0f-markersize,1.0f - 2*(pos_y+i*80)/ h-0.5f*markersize*rate,2.0f,markersize*rate,Math.abs(i)/10.0f,1.0f-Math.abs(i)/10.0f,0.0f);

        }
*/
//        drawRect(-1.0f+2*pos_x/ w - 0.5f* markersize,1.0f - 2*pos_y/ h-0.5f*markersize*rate,markersize,markersize*rate,1.0f,0.0f,0.0f);

        drawRect(1.0f - markersize,1.0f - 2*pos_y/ h-0.5f*markersize*rate,markersize,markersize*rate,1.0f,0.0f,0.0f);


        /*
        for(int i = 0;i<20;i++)
        {
            int j = i+2;
            int k = frameTotal % 20;

            if(i == k)
                drawRect(1.0f-markersize * j * rate * 0.5f,1.0f - 2*pos_y/ h-0.5f*markersize*rate,markersize*rate*0.4f,markersize*rate,0.0f,1.0f,0.0f);
            else
                drawRect(1.0f-markersize * j * rate * 0.5f,1.0f - 2*pos_y/ h-0.5f*markersize*rate,markersize*rate*0.4f,markersize*rate,1.0f,0.0f,0.0f);
        }
        */


        // MARKERS
        drawRect(-1.0f,-1.0f, 2.0f ,0.2f,0.0f,0.0f,0.0f);
        drawRect(-1.0f,0.8f, 2.0f ,0.2f,0.0f,0.0f,0.0f);


        drawRect(-1,-1,markersize,markersize*rate,1.0f,0.0f,0.0f);
        drawRect(1-markersize,-1,markersize,markersize*rate,0.0f,1.0f,0.0f);
        drawRect(-1,1-markersize*rate,markersize,markersize*rate,0.0f,0.0f,1.0f);
        drawRect(1-markersize,1-markersize*rate,markersize,markersize*rate,1.0f,1.0f,1.0f);

        drawRect(-1 + (2-markersize)*0.9f,1-markersize*rate,markersize,markersize*rate,0.0f,0.0f,1.0f);
        drawRect(-1 + (2-markersize)*0.9f,-1,markersize,markersize*rate,0.0f,0.0f,1.0f);
        drawRect(0-markersize*0.5f,-1 + (2-markersize)*0.96f,markersize,markersize*rate,0.0f,1.0f,0.0f);

        //drawRect(1-markersize,1-markersize,markersize,markersize,0.0f,0.0f,0.0f);

        if (bPartialUpdateMode && pos_y != 0) GLES20.glDisable(GLES20.GL_SCISSOR_TEST);
    }

    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        w = width;
        h = height;
    }


}

